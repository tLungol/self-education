package com.luxoft.dnepr.courses.unit1;

public class LuxoftUtils {
    public static String getMonthName(int monthOrder, String language){
        String answer;
        if (language.equalsIgnoreCase("en")){
            switch (monthOrder){
                case 1:answer = "January";break;
                case 2:answer ="February";break;
                case 3:answer ="March";break;
                case 4:answer ="April";break;
                case 5:answer ="May";break;
                case 6:answer ="June";break;
                case 7:answer ="July";break;
                case 8:answer ="August";break;
                case 9:answer ="September";break;
                case 10:answer ="October";break;
                case 11:answer ="November";break;
                case 12:answer ="December";break;
                default: answer= "Invalid month";
                    return answer;
            }
        }else{
            if ((language.equalsIgnoreCase("ru"))){
                switch (monthOrder){
                    case 1:answer = "Январьy";break;
                    case 2:answer ="Февраль";break;
                    case 3:answer ="Март";break;
                    case 4:answer ="Апрель";break;
                    case 5:answer ="Май";break;
                    case 6:answer ="Июнь";break;
                    case 7:answer ="Инюль";break;
                    case 8:answer ="Август";break;
                    case 9:answer ="Сентябрь";break;
                    case 10:answer ="Октябрь";break;
                    case 11:answer ="Ноябрь";break;
                    case 12:answer ="Декабрь";break;
                    default: answer= "Неизвестный месяц";
                        return answer;
                }

        }else{
                answer= "Unknown Language";
            }

        }
        return answer;
    }

    public static String binaryToDecimal(String binaryNumber){
        Integer temp = Integer.parseInt(binaryNumber);
        Integer  modulo;
        String answer="Not binary";
        Integer result=0;
        for (int i=0;i<=binaryNumber.length()-1;i++){
            modulo = temp%10;
            if (!(modulo.equals(1))&!(modulo.equals(0))){
                answer = "Not binary";                 
                break;
            }else{
                temp=temp/10;
                result=result+((Double) Math.pow(2,i)).intValue() *modulo;
                answer=result.toString();
            }
        }

        return (answer);
    }

    public static String decimalToBinary(String decimalNumber){
        Integer temp;
        try{
            temp = Integer.parseInt(decimalNumber);
        }catch (NumberFormatException e){
            return "Not decimal";
        }

        StringBuilder s= new StringBuilder();
        if (temp==0){
            s.append("0");
            return s.toString();
        }
        Integer modula;
        Integer quotient;
        quotient= temp/2;
        while (temp!=0){
            modula=temp%2;
            quotient= temp/2;
            s.append(modula);
            temp=quotient;             
        }
        return s.reverse().toString() ;
    }

    public static void swap(int[] array, Integer a, Integer b){
        Integer tmp=a;
        tmp=array[a];        
        array[a]=array[b];
        array[b]=tmp;
    }

    public static int[] sortArray(int[] array, boolean asc){
        int n=array.length;
        int[] tmpArray=array.clone();
        if (asc){
            for(int j=0;j<=n-1;j++){
                boolean flag=false;
                int min=j;
                for(int i=j;i<=n-j-2;i++){
                    if(tmpArray[i]>tmpArray[i+1]){
                        swap(tmpArray,i,i+1);
                        flag=true;
                    }
                    if (tmpArray[i]<tmpArray[min]){
                        min=i;
                    }
                }
                if (!flag) break;
                if (min!=j){
                    swap(tmpArray, j,min);
                }

            }
        }else {
            for(int j=0;j<=n-1;j++){
                boolean flag=false;
                int min=j;
                for(int i=j;i<=n-j-2;i++){
                    if(tmpArray[i]<tmpArray[i+1]){
                        swap(tmpArray,i,i+1);
                        flag=true;
                    }
                    if (tmpArray[i]>tmpArray[min]){
                        min=i;
                    }
                }
                if (!flag) break;
                if (min!=j){
                    swap(tmpArray, j,min);
                }
            }
        }

        return tmpArray;
    }


}
