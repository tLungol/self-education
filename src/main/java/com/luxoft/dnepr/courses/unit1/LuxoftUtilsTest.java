package com.luxoft.dnepr.courses.unit1;

import org.junit.Assert;
import org.junit.Test;

public class LuxoftUtilsTest {
    @Test
    public void caledarTest1() {
        Assert.assertEquals("January", LuxoftUtils.getMonthName(1, "en"));
        Assert.assertEquals("Декабрь", LuxoftUtils.getMonthName(12, "ru"));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(5, "hindi"));
        Assert.assertEquals("Unknown Language", LuxoftUtils.getMonthName(13, "hindi"));
        Assert.assertEquals("Неизвестный месяц", LuxoftUtils. getMonthName(-5, "ru"));

    }

    @Test
    public void binaryToDecimalTest(){
        Assert.assertEquals("Not binary", LuxoftUtils.binaryToDecimal("25212"));
        Assert.assertEquals("5", LuxoftUtils.binaryToDecimal("101"));
        Assert.assertEquals("0", LuxoftUtils.binaryToDecimal("0"));
    }

    @Test
    public void decimalToBinaryTest(){
        Assert.assertEquals("1101", LuxoftUtils.decimalToBinary("13"));
        Assert.assertEquals("1010", LuxoftUtils.decimalToBinary("10"));
        Assert.assertEquals("0", LuxoftUtils.decimalToBinary("0"));
        Assert.assertEquals("Not decimal", LuxoftUtils.decimalToBinary("ad"));
    }

    @Test
    public void sortArray(){
        int[] b=new int[]{1,2,4,5};
        Assert.assertArrayEquals(b,LuxoftUtils.sortArray(new int[]{1,4,5,2},true));
        b=new int[]{1,2,3,4,5};
        Assert.assertArrayEquals(b,LuxoftUtils.sortArray(new int[]{1,4,5,2,3},true));
        b=new int[]{5,4,3,2,1};
        Assert.assertArrayEquals(b,LuxoftUtils.sortArray(new int[]{1,4,5,2,3},false));
    }
}
