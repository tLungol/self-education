package com.luxoft.dnepr.courses.example;

public final class PureLuxoftUtils {

    private PureLuxoftUtils() { }

    /**
     * Function, which returns factorial of given number. (Recursion is used for calculations)
     * @param n input number
     * @return
     */
    public static int factorial(int n) {
        if (n < 0) {
            throw new IllegalArgumentException(String.format("Input value '%s' is less than 0", n));
        }
        if (n == 0 || n == 1) {
            return 1;
        }
        return n * factorial(n - 1);
    }

}
