package com.luxoft.dnepr.courses.unit2.model;

/**
 * Created by IntelliJ IDEA.
 * User: bulick
 * Date: 25.03.13
 * Time: 15:20
 * To change this template use File | Settings | File Templates.
 */
public class Square extends Figure{
    protected double side;

    public Square(double side){
        this.side=side;
    }
    @Override
    public double calculateArea() {
        return side*side;
    }
}
