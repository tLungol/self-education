package com.luxoft.dnepr.courses.unit2.model;

/**
 * Created by IntelliJ IDEA.
 * User: bulick
 * Date: 25.03.13
 * Time: 15:18
 * To change this template use File | Settings | File Templates.
 */
public abstract class Figure {
    public abstract double calculateArea();
}
