package com.luxoft.dnepr.courses.unit2;


import java.util.*;

import com.luxoft.dnepr.courses.unit2.model.*;

public final class LuxoftUtils {
    private LuxoftUtils() {
    }

    private static boolean asc;

    public static String[] sortArray(String[] array, boolean asc) {
        String[] tempArray = array.clone();
        LuxoftUtils.asc = asc;
        mergesort(tempArray, 0, tempArray.length - 1);
        return tempArray;
    }

    private static void mergesort(String[] array) {
        mergesort(array, 0, array.length - 1);
    }

    private static void mergesort(String[] array, int l, int r) {
        if (l >= r) return;
        int m = (l + r) / 2;
        mergesort(array, l, m);
        mergesort(array, m + 1, r);
        merge(array, l, m, r, LuxoftUtils.asc);
    }


    private static void merge(String[] array, int l, int m, int r, boolean asc) {
        if (m + 1 > r) return;
        String[] b = new String[array.length];
        for (int i = l; i != m + 1; i++) {
            b[i] = array[i];
        }
        for (int i = m + 1; i != r + 1; i++) {
            b[i] = array[r + m + 1 - i];
        }
        int k = l;
        int j = r;
        for (int i = l; i != r + 1; i++) {
            if (needToSwap(b[k], b[j], LuxoftUtils.asc)) array[i] = b[k++];
            else array[i] = b[j--];
        }
    }

    public static boolean needToSwap(String a1, String a2, boolean asc) {
        a2 = a2.trim();
        a1 = a1.trim();
        return asc ? a2.compareTo(a1) <= 0 : a1.compareTo(a2) <= 0;
    }

    public static double wordAverageLength(String str) {
        str = str.trim();
        int wordCounter = 0;
        int spaceCounter = 0;
        for (int i = 0; i < str.length(); i++) {
            if ((str.charAt(i)) == ' ') {
                spaceCounter++;
                if (str.charAt(i - 1) != ' ') wordCounter++;
            }
        }
        if (wordCounter == 0) return 0;

        return (double) (str.length() - spaceCounter) / (wordCounter + 1);
    }

    public static String reverseWords(String str) {
        StringBuilder tmp = new StringBuilder(str);
        for (int i = 0; i <= tmp.length() - 1; i++) {
            if (tmp.charAt(i) != ' ') {
                int startIndex = i;
                int endsIndex = tmp.length();
                for (int j = i; j <= tmp.length() - 1; j++) {
                    i = j;
                    if (tmp.charAt(j) == ' ') {
                        endsIndex = j;
                        break;
                    }
                }
                tmp.replace(startIndex, endsIndex, new StringBuilder(tmp.substring(startIndex, endsIndex)).reverse().toString());
            }
        }

        return tmp.toString();
    }

    public static char[] getCharEntries(String str) {
        class MyChar implements Comparable<MyChar> {

            public boolean equals(Object o) {
                if (o.getClass().equals(this.getClass())) {
                    if (this.character == ((MyChar) o).getCharacter()) return true;
                }
                return false;
            }

            public MyChar(char character) {
                this.character = character;
                this.counter = 1;
            }


            private char character;
            private int counter;

            public int getCounter() {
                return counter;
            }

            public void setCounter(int counter) {
                this.counter = counter;
            }

            public void addChar() {
                this.counter++;
            }

            public char getCharacter() {
                return character;
            }

            public void setCharacter(char character) {
                this.character = character;
            }


            @Override
            public int compareTo(MyChar o) {
                return (o.getCounter() < this.getCounter()) ? -1 : ((this.getCounter() == o.getCounter()) ? Character.compare(this.getCharacter(), o.getCharacter()) : 1);
            }
        }
        boolean isFound;
        //Set<MyChar> array=new TreeSet<MyChar>();
        List<MyChar> array = new ArrayList<MyChar>();
        for (int i = 0; i <= str.length() - 1; i++) {
            isFound = false;
            if (Character.isAlphabetic(str.codePointAt(i))) {
                if (array.size() == 0) {
                    array.add(new MyChar(str.charAt(i)));
                    continue;
                }
                Iterator<MyChar> it = array.iterator();
                while (it.hasNext()) {
                    // for (int j = 0; j < array.size(); j++) {
                    MyChar c = it.next();
                    if ((c.getCharacter()) == str.charAt(i)) {
                        c.addChar();
                        isFound = true;
                        break;
                    }
                }
                if (!isFound) {
                    array.add(new MyChar(str.charAt(i)));

                }

            }

        }

        Collections.sort(array);
        char[] out = new char[array.size()];
        int i = 0;
        Iterator<MyChar> it = array.iterator();
        while (it.hasNext()) {
            out[i] = it.next().getCharacter();
            i++;
        }

        return out;
    }

    public static double calculateOverallArea(List<Figure> figures) {
        Iterator<Figure> it = figures.iterator();
        double sum = 0;
        while (it.hasNext()) {
            sum = sum + it.next().calculateArea();
        }
        return sum;
    }

}
