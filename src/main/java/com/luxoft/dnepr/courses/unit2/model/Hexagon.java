package com.luxoft.dnepr.courses.unit2.model;

/**
 * Created by IntelliJ IDEA.
 * User: bulick
 * Date: 25.03.13
 * Time: 15:21
 * To change this template use File | Settings | File Templates.
 */
public class Hexagon extends Figure{
    protected double side;
    public Hexagon (double side){
        this.side=side;
    }
    @Override
    public double calculateArea() {
        return 3*Math.pow(3,0.333333333)*Math.pow(this.side,2)/2;
    }
}
