package com.luxoft.dnepr.courses.unit2.model;

import java.awt.image.PixelGrabber;

/**
 * Created by IntelliJ IDEA.
 * User: bulick
 * Date: 25.03.13
 * Time: 15:21
 * To change this template use File | Settings | File Templates.
 */
public class Circle extends Figure{
    protected double radius;
    public Circle (double radius) {
        this.radius=radius;
    }
    @Override
    public double calculateArea() {
        return Math.PI*Math.pow(this.radius,2)/2;
    }
}
