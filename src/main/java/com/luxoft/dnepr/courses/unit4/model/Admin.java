package com.luxoft.dnepr.courses.unit4.model;


public class Admin extends ITWorker{
    public Admin() {
        super();
        this.beerBrand = null;
    }

    public Admin(String firstname, String lastname, String phone, String company, String gender, String[] skills, String beerBrand) {
        super(firstname, lastname, phone, company, gender, skills);
        this.beerBrand = beerBrand;
    }

    public String getBeerBrand() {
        return beerBrand;
    }

    public void setBeerBrand(String beerBrand) {
        if (beerBrand != null && !beerBrand.equals("")){
            this.beerBrand = beerBrand;
        }else {
            throw new IllegalArgumentException();
        }
    }

    public StringBuilder toXML(){
        return super.toXML().insert(0,"<admin>").append("<beerbrand>"+this.beerBrand+"</beerbrand></admin>");
    }

    private String beerBrand;
}
