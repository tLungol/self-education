package com.luxoft.dnepr.courses.unit4.model;


public class QA extends  ITWorker{
    public String getHairColor() {
        return hairColor;
    }

    public void setHairColor(String hairColor) {
        this.hairColor = hairColor;
    }

    private String hairColor;
    public StringBuilder toXML(){
        return super.toXML().append("<haircolor>"+this.hairColor+"</haircolor>");
    }
}
