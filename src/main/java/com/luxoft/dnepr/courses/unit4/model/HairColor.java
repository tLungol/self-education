package com.luxoft.dnepr.courses.unit4.model;


public enum HairColor {
    DARK,
    BLONDE,
    REDHAD

}
