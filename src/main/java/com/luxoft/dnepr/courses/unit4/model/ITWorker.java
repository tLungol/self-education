package com.luxoft.dnepr.courses.unit4.model;


/*
*String firstname, String lastname, String phone, String company, String gender с двумя разрешенными значениями
*('m' или 'f'), String[] skills).
*/
public abstract class ITWorker {
    private String firstname;
    private String lastname;
    private String phone;
    private String company;
    private String gender;

    protected ITWorker(String firstname, String lastname, String phone, String company, String gender, String[] skills) {
        setFirstname( firstname);
        setLastname(lastname);
        setPhone(phone);
        setCompany(company);
        setGender(gender);
        setSkills(skills);
    }

    protected ITWorker() {
        this.firstname = null;
        this.lastname = null;
        this.phone = null;
        this.company = null;
        this.gender = null;
        this.skills = null;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String[] getSkills() {
        return skills;
    }

    public void setSkills(String[] skills) {
        this.skills = skills;
    }

    private String[] skills;
    public static String stringPrinter(String[] skills){
        if (skills==null || skills.length==0){
            return "";
        }
        StringBuilder output = new StringBuilder();

        for (String s:skills){
            output.append(s+"_");
        }

        return output.toString();
    }

    public StringBuilder toXML (){
        StringBuilder str = new StringBuilder();
        str.append("<firstname>");
        str.append(this.firstname);
        str.append("</firstname><lastname>");
        str.append(this.lastname);
        str.append("</lastname><phone>");
        str.append(this.phone);
        str.append("</phone><company>");
        str.append(this.company);
        str.append("</company><gender>");
        str.append(this.gender);
        str.append("</gender><skills>");
        if (this.skills!=null){
            for(String s:this.skills){
                str.append("<skill>"+s+"</skill>");
            }
        }    
         str.append("</skills>");
        return str;
    }

    
}
