package com.luxoft.dnepr.courses.unit4.model;
import com.sun.xml.internal.stream.Entity;

import java.util.LinkedList;
import java.util.Scanner;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.*;

public final class LuxoftWorkerStorage {
    public static void serialize(List<ITWorker> workers, String absolutePathToFile) throws IOException {
        BufferedWriter d= null;
        try {
            d = new BufferedWriter(new FileWriter(absolutePathToFile));
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (ITWorker worker:workers){
            if (worker instanceof Admin){
                Admin admin=(Admin)worker;
                d.write(admin.toXML().append("\n").toString());

            }    
            if (worker instanceof BA){
                BA ba=(BA)worker;
                d.write(worker.toXML().append("\n").toString());
            }
            if (worker instanceof Developer){
                Developer developer=(Developer)worker;
                d.write(developer.toXML().append("\n").toString());
            }
            if (worker instanceof PM){
                PM pm=(PM)worker;
                d.write(pm.toXML().append("\n").toString());
            }
            if (worker instanceof QA){
                QA qa=(QA)worker;
                d.write(qa.toXML().append("\n").toString());
            }

        }
        d.close();
    }

    public static List<ITWorker> deserialize(String absolutePathToFile) throws FileNotFoundException {
        Scanner scanner = new Scanner(new File(absolutePathToFile));
        StringBuilder s=new StringBuilder();
        List<ITWorker> array = new ArrayList<ITWorker>();
        while (scanner.hasNext()){
            s.delete(0,s.length());
            s.append(scanner.nextLine()) ;
            Matcher matcher = Pattern.compile("(admin)(?=\\>)").matcher(s.toString());
            matcher.find();
            if (matcher.group().equals("admin")){
                String[] spl=s.toString().split("<[\\/]?admin>");
                array.add(new Admin(spl[1].split("<[\\/]?firstname>")[1], spl[1].split("<[\\/]?lastname>")[1],spl[1].split("<[\\/]?phone>")[1],
                        spl[1].split("<[\\/]?company>")[1],spl[1].split("<[\\/]?gender>")[1],skillMatcher(spl[1].split("<[\\/]?skills>")[1]),spl[1].split("<[\\/]?beerbrand>")[1]));
            }
            if (matcher.group().equals("ba")){
                String[] spl=s.toString().split("<[\\/]?ba>");
                array.add(new Admin(spl[1].split("<[\\/]?firstname>")[1], spl[1].split("<[\\/]?lastname>")[1],spl[1].split("<[\\/]?phone>")[1],
                        spl[1].split("<[\\/]?company>")[1],spl[1].split("<[\\/]?gender>")[1],skillMatcher(spl[1].split("<[\\/]?skills>")[1]),spl[1].split("<[\\/]?favoritefood>")[1]));
            }

            if (matcher.group().equals("developer")){
                String[] spl=s.toString().split("<[\\/]?ba>");
                array.add(new Admin(spl[1].split("<[\\/]?firstname>")[1], spl[1].split("<[\\/]?lastname>")[1],spl[1].split("<[\\/]?phone>")[1],
                        spl[1].split("<[\\/]?company>")[1],spl[1].split("<[\\/]?gender>")[1],skillMatcher(spl[1].split("<[\\/]?skills>")[1]),spl[1].split("<[\\/]?experience>")[1]));
            }
            if (matcher.group().equals("pm")){
                String[] spl=s.toString().split("<[\\/]?pm>");
                array.add(new Admin(spl[1].split("<[\\/]?firstname>")[1], spl[1].split("<[\\/]?lastname>")[1],spl[1].split("<[\\/]?phone>")[1],
                        spl[1].split("<[\\/]?company>")[1],spl[1].split("<[\\/]?gender>")[1],skillMatcher(spl[1].split("<[\\/]?skills>")[1]),spl[1].split("<[\\/]?countofprojects>")[1]));
            }
            if (matcher.group().equals("qa")){
                String[] spl=s.toString().split("<[\\/]?qa>");
                array.add(new Admin(spl[1].split("<[\\/]?firstname>")[1], spl[1].split("<[\\/]?lastname>")[1],spl[1].split("<[\\/]?phone>")[1],
                        spl[1].split("<[\\/]?company>")[1],spl[1].split("<[\\/]?gender>")[1],skillMatcher(spl[1].split("<[\\/]?skills>")[1]),spl[1].split("<[\\/]?haircolor>")[1]));
            }

        }
        return array;
        }

    public static String[] skillMatcher(String str){
        String[] spl = str.split("<[\\/]?skill>");
        String[] array = new String[spl.length/2];
        int j=0;
       for (int i=1;i<=spl.length-1;i=i+2){
           array[j]=spl[i];
           j++;
       }
        return array;
    }

    public static void main(String [ ] args)
    {   List<ITWorker> array = new ArrayList<ITWorker>();
        Admin admin = new Admin();
        admin.setFirstname("asd");
        admin.setLastname("asgh");
        admin.setCompany("LUXOFT");
        admin.setGender("male");
        admin.setPhone("4654654132");
        admin.setSkills(new String[]{"asdfs", "adsf", "afdghjkljgfd"});
        admin.setBeerBrand("BUD");
        array.add(admin);
        array.add(new PM("taras","lungol","5552467","luxoft","famale",new String[]{"asdf","asdf"},4 ));
        try {
            serialize(array,"d:\\unit.txt");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            array=deserialize("d:\\unit.txt") ;
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            serialize(array,"d:\\unit1.txt");
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

}
