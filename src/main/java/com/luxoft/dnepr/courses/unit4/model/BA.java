package com.luxoft.dnepr.courses.unit4.model;

public class BA extends ITWorker{
    public String getFavoriteFood() {
        return favoriteFood;
    }

    public void setFavoriteFood(String favoriteFood) {
        if (favoriteFood!=null && !favoriteFood.equals("")){
            this.favoriteFood = favoriteFood;
        }else {
            throw new IllegalArgumentException();
        }

    }

    private String favoriteFood;
    public StringBuilder toXML(){
        return super.toXML().append("<favoritefood>"+this.favoriteFood+"</favoritefood>");
    }
}
