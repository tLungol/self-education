package com.luxoft.dnepr.courses.unit4.model;


public class Developer extends ITWorker{

    private double  experience;
    public double getExperience() {
        return experience;
    }

    public void setExperience(double experience) {
        if (experience>=0){
            this.experience = experience;
        }else {
            throw new IllegalArgumentException();
        }


    }
    public StringBuilder toXML(){
        return super.toXML().append("<experience>"+this.experience+"</experience>");
    }


}
