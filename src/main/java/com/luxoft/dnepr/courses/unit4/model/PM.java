package com.luxoft.dnepr.courses.unit4.model;


public class PM extends ITWorker{
    public PM(String firstname, String lastname, String phone, String company, String gender, String[] skills, int countOfProjects) {
        super(firstname, lastname, phone, company, gender, skills);
        this.countOfProjects = countOfProjects;
    }

    public PM(int countOfProjects) {
        this.countOfProjects = countOfProjects;
    }

    public int getCountOfProjects() {
        return countOfProjects;
    }

    public void setCountOfProjects(int countOfProjects) {
        if (countOfProjects>=0){
            this.countOfProjects = countOfProjects;
        }else {
            throw new IllegalArgumentException();
        }

    }

    private int countOfProjects;
    public StringBuilder toXML(){
        return super.toXML().append("<countofprojects>"+this.countOfProjects+"</countofprojects>");
    }
}
