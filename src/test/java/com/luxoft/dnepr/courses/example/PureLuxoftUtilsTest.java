package com.luxoft.dnepr.courses.example;

import org.junit.Assert;
import org.junit.Test;

public class PureLuxoftUtilsTest {

    @Test
    public void factorialTest1() {
        Assert.assertEquals(1, PureLuxoftUtils.factorial(0));
        Assert.assertEquals(1, PureLuxoftUtils.factorial(1));
        Assert.assertEquals(2, PureLuxoftUtils.factorial(2));
        Assert.assertEquals(6, PureLuxoftUtils.factorial(3));
        Assert.assertEquals(24, PureLuxoftUtils.factorial(4));
        Assert.assertEquals(120, PureLuxoftUtils.factorial(5));
    }

    @Test
    public void factorialIllegalArgumentTest() {
        try {
            PureLuxoftUtils.factorial(-1);
            Assert.fail("-1 must invoke IllegalArgumentException within PureLuxoftUtils.factorial(int n) method.");
        } catch(IllegalArgumentException e) {
            Assert.assertEquals("Input value '-1' is less than 0", e.getMessage());
        }
        try {
            PureLuxoftUtils.factorial(-50);
            Assert.fail("-50 must invoke IllegalArgumentException within PureLuxoftUtils.factorial(int n) method.");
        } catch(IllegalArgumentException e) {
            Assert.assertEquals("Input value '-50' is less than 0", e.getMessage());
        }
    }

}
