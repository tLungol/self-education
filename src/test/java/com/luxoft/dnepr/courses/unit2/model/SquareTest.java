package com.luxoft.dnepr.courses.unit2.model;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by IntelliJ IDEA.
 * User: bulick
 * Date: 25.03.13
 * Time: 17:57
 * To change this template use File | Settings | File Templates.
 */
public class SquareTest {
    @Test
    public void testCalculateArea() throws Exception {
        Assert.assertEquals(25, new Square(5).calculateArea(), 0.000001);
    }
}
