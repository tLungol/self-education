package com.luxoft.dnepr.courses.unit2.model;


import org.junit.Assert;
import org.junit.Test;

/**
 * Created by IntelliJ IDEA.
 * User: bulick
 * Date: 25.03.13
 * Time: 17:14
 * To change this template use File | Settings | File Templates.
 */
public class CircleTest {

    @Test
    public void calculateAreaTest()throws Exception {

        Assert.assertEquals(39.269908169872415480783042290994,new Circle(5).calculateArea(),0.000001);

    }
}
