package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;
import com.luxoft.dnepr.courses.unit2.model.Square;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created with IntelliJ IDEA.
 * User: anthony
 * Date: 3/24/13
 * Time: 4:39 PM
 * To change this template use File | Settings | File Templates.
 */
public class _LuxoftUtilsTest {

    private static double DELTA = 0.000001;

    @Test
    public void sortArrayAscTest() {
        Assert.assertArrayEquals(new String[]{"a", "b"}, LuxoftUtils.sortArray(new String[]{"b", "a"}, true));
        Assert.assertArrayEquals(new String[]{"a", "b"}, LuxoftUtils.sortArray(new String[]{"a", "b"}, true));

        Assert.assertArrayEquals(new String[]{"a", "b", "c"}, LuxoftUtils.sortArray(new String[]{"c", "b", "a"}, true));
        Assert.assertArrayEquals(new String[]{"a", "b", "c"}, LuxoftUtils.sortArray(new String[]{"c", "a", "b"}, true));
        Assert.assertArrayEquals(new String[]{"a", "b", "c"}, LuxoftUtils.sortArray(new String[]{"b", "c", "a"}, true));
        Assert.assertArrayEquals(new String[]{"a", "b", "c"}, LuxoftUtils.sortArray(new String[]{"a", "c", "b"}, true));
        Assert.assertArrayEquals(new String[]{"a", "b", "c"}, LuxoftUtils.sortArray(new String[]{"a", "b", "c"}, true));
        Assert.assertArrayEquals(new String[]{"a", "b", "c"}, LuxoftUtils.sortArray(new String[]{"b", "a", "c"}, true));

        Assert.assertArrayEquals(new String[]{"A", "a", "b", "c", "d", "e"}, LuxoftUtils.sortArray(new String[]{"d", "e", "b", "a", "c", "A"}, true));
    }

    @Test
    public void sortArrayDescTest() {
//        Assert.assertArrayEquals(new String[]{"b", "a"}, LuxoftUtils.sortArray(new String[]{"b", "a"}, false));
//        Assert.assertArrayEquals(new String[]{"b", "a"}, LuxoftUtils.sortArray(new String[]{"a", "b"}, false));

        Assert.assertArrayEquals(new String[]{"c", "b", "a"}, LuxoftUtils.sortArray(new String[]{"c", "b", "a"}, false));
        Assert.assertArrayEquals(new String[]{"c", "b", "a"}, LuxoftUtils.sortArray(new String[]{"c", "a", "b"}, false));
        Assert.assertArrayEquals(new String[]{"c", "b", "a"}, LuxoftUtils.sortArray(new String[]{"b", "c", "a"}, false));
        Assert.assertArrayEquals(new String[]{"c", "b", "a"}, LuxoftUtils.sortArray(new String[]{"a", "c", "b"}, false));
        Assert.assertArrayEquals(new String[]{"c", "b", "a"}, LuxoftUtils.sortArray(new String[]{"a", "b", "c"}, false));
        Assert.assertArrayEquals(new String[]{"c", "b", "a"}, LuxoftUtils.sortArray(new String[]{"b", "a", "c"}, false));

        Assert.assertArrayEquals(new String[]{"e", "d", "c", "b", "a", "A"}, LuxoftUtils.sortArray(new String[]{"d", "e", "b", "a", "c", "A"}, false));
    }

    @Test
    public void wordAverageLengthSingleSpacesTest() {
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("I have a cat"), DELTA);
        Assert.assertEquals(3, LuxoftUtils.wordAverageLength("I have a parrot"), DELTA);
        Assert.assertEquals(3, LuxoftUtils.wordAverageLength("aaa bbb ccc"), DELTA);
        Assert.assertEquals(2.5, LuxoftUtils.wordAverageLength("a aa aaa aaaa"), DELTA);
        Assert.assertEquals(8, LuxoftUtils.wordAverageLength("ZZZ zzzzzzzzzzzzz"), DELTA);
    }

    @Test
    public void wordAverageLengthMultipleSpacesInsideTest() {
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("I    have    a    cat"), DELTA);
        Assert.assertEquals(3, LuxoftUtils.wordAverageLength("I   have     a     parrot"), DELTA);
        Assert.assertEquals(3, LuxoftUtils.wordAverageLength("aaa       bbb       ccc"), DELTA);
        Assert.assertEquals(2.5, LuxoftUtils.wordAverageLength("a    aa     aaa      aaaa"), DELTA);
        Assert.assertEquals(8, LuxoftUtils.wordAverageLength("ZZZ       zzzzzzzzzzzzz"), DELTA);
    }

    @Test
    public void wordAverageLengthMultipleSpacesBeforeTest() {
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("  I    have    a    cat"), DELTA);
        Assert.assertEquals(3, LuxoftUtils.wordAverageLength(" I   have     a     parrot"), DELTA);
        Assert.assertEquals(3, LuxoftUtils.wordAverageLength("    aaa       bbb       ccc"), DELTA);
        Assert.assertEquals(2.5, LuxoftUtils.wordAverageLength(" a    aa     aaa      aaaa"), DELTA);
        Assert.assertEquals(8, LuxoftUtils.wordAverageLength(" ZZZ       zzzzzzzzzzzzz"), DELTA);
    }

    @Test
    public void wordAverageLengthMultipleSpacesAfterTest() {
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength("I    have    a    cat "), DELTA);
        Assert.assertEquals(3, LuxoftUtils.wordAverageLength("I   have     a     parrot  "), DELTA);
        Assert.assertEquals(3, LuxoftUtils.wordAverageLength("aaa       bbb       ccc   "), DELTA);
        Assert.assertEquals(2.5, LuxoftUtils.wordAverageLength("a    aa     aaa      aaaa    "), DELTA);
        Assert.assertEquals(8, LuxoftUtils.wordAverageLength("ZZZ       zzzzzzzzzzzzz     "), DELTA);
    }

    @Test
    public void wordAverageLengthMultipleSpacesEveryWhereTest() {
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength(" I    have    a    cat "), DELTA);
        Assert.assertEquals(3, LuxoftUtils.wordAverageLength("  I   have     a     parrot  "), DELTA);
        Assert.assertEquals(3, LuxoftUtils.wordAverageLength("   aaa       bbb       ccc   "), DELTA);
        Assert.assertEquals(2.5, LuxoftUtils.wordAverageLength("    a    aa     aaa      aaaa    "), DELTA);
        Assert.assertEquals(8, LuxoftUtils.wordAverageLength("     ZZZ       zzzzzzzzzzzzz     "), DELTA);
    }

    @Test
    public void reverseWordsTest() {
        Assert.assertEquals("cba", LuxoftUtils.reverseWords("abc"));
        Assert.assertEquals("cba edc", LuxoftUtils.reverseWords("abc cde"));
        Assert.assertEquals("cba edc cba", LuxoftUtils.reverseWords("abc cde abc"));
        Assert.assertEquals("cba edc sCA", LuxoftUtils.reverseWords("abc cde ACs"));
    }

    @Test
    public void reverseWordsMultipleWhitespacesTest() {
        Assert.assertEquals("cba   edc", LuxoftUtils.reverseWords("abc   cde"));
        Assert.assertEquals("cba  edc   cba", LuxoftUtils.reverseWords("abc  cde   abc"));
        Assert.assertEquals("cba   edc  sCA", LuxoftUtils.reverseWords("abc   cde  ACs"));
    }

    @Test
    public void reverseWordsWhitespacesBeforeTest() {
        Assert.assertEquals(" cba   edc", LuxoftUtils.reverseWords(" abc   cde"));
        Assert.assertEquals("  cba  edc   cba", LuxoftUtils.reverseWords("  abc  cde   abc"));
        Assert.assertEquals("   cba   edc  sCA", LuxoftUtils.reverseWords("   abc   cde  ACs"));
    }

    @Test
    public void reverseWordsWhitespacesAfterTest() {
        Assert.assertEquals("cba   edc ", LuxoftUtils.reverseWords("abc   cde "));
        Assert.assertEquals("cba  edc   cba  ", LuxoftUtils.reverseWords("abc  cde   abc  "));
        Assert.assertEquals("cba   edc  sCA   ", LuxoftUtils.reverseWords("abc   cde  ACs   "));
    }

    @Test
    public void reverseWordsMultipleWhitespacesEverywhereTest() {
        Assert.assertEquals("   cba   edc   ", LuxoftUtils.reverseWords("   abc   cde   "));
        Assert.assertEquals("  cba  edc   cba  ", LuxoftUtils.reverseWords("  abc  cde   abc  "));
        Assert.assertEquals(" cba   edc  sCA   ", LuxoftUtils.reverseWords(" abc   cde  ACs   "));
    }

    @Test
    public void getCharEntriesTest() {
        Assert.assertArrayEquals(new char[]{'c', 'a', 'b', 'd', 'e'}, LuxoftUtils.getCharEntries("   abc   cde   "));
        Assert.assertArrayEquals(new char[]{'A', 'Z', 'a', 'b', 'c'}, LuxoftUtils.getCharEntries("   cba   AAA ZZ   "));
        Assert.assertArrayEquals(new char[]{'A', 'S', 'a', 'z'}, LuxoftUtils.getCharEntries("   aaa AAA zzz SSS   "));
        Assert.assertArrayEquals(new char[]{'s', 'A', 'B', 'C', 'S', 'a', 'b', 'c'}, LuxoftUtils.getCharEntries("aAbBcCssS"));
    }

    @Test
    public void calculateOverallAreaSquaresTest() {
        List<Figure> figures = new ArrayList<Figure>();
        Assert.assertEquals(0, LuxoftUtils.calculateOverallArea(figures), DELTA);
        figures.add(new Square(2));
        Assert.assertEquals(4, LuxoftUtils.calculateOverallArea(figures), DELTA);
        figures.add(new Square(1));
        Assert.assertEquals(5, LuxoftUtils.calculateOverallArea(figures), DELTA);
        figures.add(new Square(3.3));
        Assert.assertEquals(15.89, LuxoftUtils.calculateOverallArea(figures), DELTA);
    }

    @Test
    public void calculateOverallAreaCirclesTest() {
        List<Figure> figures = new ArrayList<Figure>();
        Assert.assertEquals(0, LuxoftUtils.calculateOverallArea(figures), DELTA);
        figures.add(new Circle(2 / Math.sqrt(Math.PI)));
        Assert.assertEquals(4, LuxoftUtils.calculateOverallArea(figures), DELTA);
        figures.add(new Circle(1 / Math.sqrt(Math.PI)));
        Assert.assertEquals(5, LuxoftUtils.calculateOverallArea(figures), DELTA);
        figures.add(new Circle(3.3 / Math.sqrt(Math.PI)));
        Assert.assertEquals(15.89, LuxoftUtils.calculateOverallArea(figures), DELTA);
    }

    @Test
    public void calculateOverallAreaHexagonsTest() {
        List<Figure> figures = new ArrayList<Figure>();
        Assert.assertEquals(0, LuxoftUtils.calculateOverallArea(figures), DELTA);
        figures.add(new Hexagon(2 * Math.sqrt(2d / Math.sqrt(27))));
        Assert.assertEquals(4, LuxoftUtils.calculateOverallArea(figures), DELTA);
        figures.add(new Hexagon(1 *  Math.sqrt(2d / Math.sqrt(27))));
        Assert.assertEquals(5, LuxoftUtils.calculateOverallArea(figures), DELTA);
        figures.add(new Hexagon(3.3 *  Math.sqrt(2d / Math.sqrt(27))));
        Assert.assertEquals(15.89, LuxoftUtils.calculateOverallArea(figures), DELTA);
    }

}
