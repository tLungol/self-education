package com.luxoft.dnepr.courses.unit2.model;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by IntelliJ IDEA.
 * User: bulick
 * Date: 25.03.13
 * Time: 17:25
 * To change this template use File | Settings | File Templates.
 */
public class HexagonTest {
    @Test
    public void testCalculateArea() throws Exception {
        Assert.assertEquals(54.084358886527814337061436654254, new Hexagon(5).calculateArea(), 0.000001);
    }
}
