package com.luxoft.dnepr.courses.unit2;

import com.luxoft.dnepr.courses.unit2.model.Circle;
import com.luxoft.dnepr.courses.unit2.model.Figure;
import com.luxoft.dnepr.courses.unit2.model.Hexagon;
import com.luxoft.dnepr.courses.unit2.model.Square;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: bulick
 * Date: 25.03.13
 * Time: 12:51
 * To change this template use File | Settings | File Templates.
 */
public class LuxoftUtilsTest {
    @Test
    public void testSortArray() throws Exception {

    }

    @Test
    public void testMergesort() throws Exception {

    }


    @Test
    public void GetCharEntriesTest() throws Exception {
        Assert.assertArrayEquals(new char[]{'a','I','c','e','h','t','v',},LuxoftUtils.getCharEntries("I have a cat"));
        Assert.assertArrayEquals(new char[]{'A','R','a','b','o','t','N','e','i','p'},LuxoftUtils.getCharEntries(" boN aapeti boRR AAt"));
    }
    @Test
    public void WordAverageLengthTest() throws Exception {
        Assert.assertEquals(0.0, LuxoftUtils.wordAverageLength(""), 0.01);
        Assert.assertEquals(0.0, LuxoftUtils.wordAverageLength(" "),0.01);
        Assert.assertEquals(4.0, LuxoftUtils.wordAverageLength("     mama       myla ramu "),0.01);
        Assert.assertEquals(3.75, LuxoftUtils.wordAverageLength(" mama   myla мам  ramu "),0.01);
        Assert.assertEquals(2.25, LuxoftUtils.wordAverageLength(" I have a cat "),0.01);
    }
    @Test
    public void reverseWordsTest() throws Exception {
        Assert.assertEquals(" amam alym umar ", LuxoftUtils.reverseWords(" mama myla ramu "));
        Assert.assertEquals("modnar  deretne    sdrow", LuxoftUtils.reverseWords("random  entered    words"));
        Assert.assertEquals("cba trf rgrg ", LuxoftUtils.reverseWords("abc frt grgr "));
    }

    @Test
    public void calculateOverallAreaTest()throws Exception{
        List<Figure> figures = new ArrayList<Figure>();
        figures.add(new Circle(1));
        figures.add(new Hexagon(1));
        figures.add(new Square(1));
        Assert.assertEquals(4.734170681463773, LuxoftUtils.calculateOverallArea(figures),0.000001);

    }
    @Test
    public void sortArrayTest() throws Exception{
        String[] array =new String[]{" Hello all!", " Amav asd"," amav asd ","Zfgfg sdafg "};
        Assert.assertArrayEquals(new String[]{" Amav asd"," Hello all!","Zfgfg sdafg "," amav asd "},LuxoftUtils.sortArray(array,false));
    }
    

}
